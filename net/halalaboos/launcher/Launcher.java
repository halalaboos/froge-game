package net.halalaboos.launcher;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

import javax.swing.JButton;
import javax.swing.JFrame;

import net.halalaboos.engine.utils.SystemUtilities;
import net.halalaboos.engine.utils.factory.Factory;

public class Launcher extends JFrame implements ActionListener {
	
	private final SystemUtilities utils = Factory.getSystemUtilities();
	
	private final String[] jarNames = new String[] {
			"game.jar",
			"lwjgl.jar",
			"lwjgl-util.jar",
			"jinput.jar",
			"slick-util.jar"
	};
	
	private String mainClass = "net.halalaboos.engine.Launcher";
	
	private String mainFunction = "run";
	
	public Launcher() {
		super("Launcher");
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.addComponents();
		this.setSize(new Dimension(640, 480));
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	private void addComponents() {
		JButton launch = new JButton("Launch");
		launch.addActionListener(this);
		add(launch, BorderLayout.SOUTH);
	}
	
	
	public static void main(String[] args) {
		Launcher launcher = new Launcher();
		// launcher.actionPerformed(null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		URLClassLoader loader = new URLClassLoader(getJars());
		try {
			System.setProperty("org.lwjgl.librarypath", getNativeFolder().getAbsolutePath());
			Class<?> clazz = loader.loadClass(mainClass);
			final Method method = clazz.getMethod(this.mainFunction);
			new Thread(){
				@Override
				public void run() {
					try {
						method.invoke(null, (Object[]) null);
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}

				}
			}.start();
			dispose();
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (NoSuchMethodException e1) {
			e1.printStackTrace();
		} catch (SecurityException e1) {
			e1.printStackTrace();
		}
	}

	private URL[] getJars() {
		URL[] urls = new URL[jarNames.length];
		for (int i = 0; i < jarNames.length; i++)
			try {
				urls[i] = new File(getJarFolder(), jarNames[i]).toURI().toURL();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		return urls;
	}
	
	private File getNativeFolder() {
		File file = new File(getGameFolder(), "natives");
		if (!file.exists())
			file.mkdirs();
		return file;
	}
	
	private File getJarFolder() {
		File file = new File(getGameFolder(), "jars");
		if (!file.exists())
			file.mkdirs();
		return file;
	}
	
	private File getGameFolder() {
		File file = new File(".");
		if (!file.exists())
			file.mkdirs();
		return file;
	}
}
