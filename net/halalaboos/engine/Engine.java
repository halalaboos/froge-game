package net.halalaboos.engine;

import java.io.IOException;
import java.nio.ByteBuffer;
import net.halalaboos.engine.rendering.GLUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

import static org.lwjgl.opengl.GL11.*;

public final class Engine {
	
	public static final Engine instance = new Engine();
	
	public static final float UPDATE_MS = 16F;

	private static final String TITLE = "Game";
		
	private boolean vsync = true, fullscreen = false;
	
	private float fps;
		
	private Scene current = null;
	
	private Vbo vao;
	
	private Engine() {
		
	}
	
	private void setupDisplay() {
		try {
			Display.setVSyncEnabled(vsync);
			Display.setFullscreen(fullscreen);
			Display.setDisplayMode(new DisplayMode(800, 600));
			Display.setResizable(true);
			Display.setTitle(TITLE);
			Display.setIcon(new ByteBuffer[] {
					GLUtils.loadIcon("res/icon16x16.png"),
					GLUtils.loadIcon("res/icon32x32.png"),
					GLUtils.loadIcon("res/icon64x64.png"),
					GLUtils.loadIcon("res/icon128x128.png")
                    });
			Display.create();
			setupMatrix();
			glViewport(0, 0, Display.getWidth(), Display.getHeight());
		} catch (LWJGLException e) {
			e.printStackTrace();
			Display.destroy();
			System.exit(-1);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void run() {
		setupDisplay();
		setup();
		loop();
		cleanup();
	}
	
	private void cleanup() {
		GLUtils.cleanup();
	}
	
	private void setup() {
		current.setup(this);
	}
	
	private void loop() {
		long lastTime = getTime(), lastFPS = getTime();
		int fpsCounter = 0;
		while (!Display.isCloseRequested()) {
			long time = getTime();
			if (Display.wasResized()) {
				resize();
			}
			
			// fps calculations
			if (time - lastFPS >= 1000) {
				fps = fpsCounter;
				fpsCounter = 1;
				lastFPS = time;
			} else
				fpsCounter++;
			
			// game updates
			float delta = (time - lastTime) / UPDATE_MS;
			if (delta >= 1F) {
				lastTime = time;
				update(delta);
			}
			
			input();
			render();
			Display.update();
		}
	}
	
	private void resize() {
		setupMatrix();
		glViewport(0, 0, Display.getWidth(), Display.getHeight());
		current.resize(Display.getWidth(), Display.getHeight());
	}
		
	public void update(float delta) {
		current.update(delta);
	}
		
	private void render() {
		current.render(vao);
	}
	
	private void setupMatrix() {
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, Display.getWidth(), Display.getHeight(), 0, 1, -1);
		glMatrixMode(GL_MODELVIEW);
	}
		
	private void input() {
		while (Keyboard.next()) {
			if (Keyboard.getEventKeyState()) {
				int keyCode = Keyboard.getEventKey();
				if (keyCode == Keyboard.KEY_F1) {
					this.toggleVsync();
				} else if (keyCode == Keyboard.KEY_F2) {
					this.toggleFullscreen();
				} else {
					current.keyTyped(keyCode, Keyboard.getEventCharacter());
				}
			}
		}
		while (Mouse.next()) {
			if (Mouse.getEventButton() == -1) {
				if (Mouse.hasWheel()) {
					int wheel = Mouse.getEventDWheel();
					if (wheel > 0)
						wheel = 1;
					else if (wheel < 0)
						wheel = -1;
					if (wheel != 0)
						current.mouseWheel(wheel);
				}
			} else {
				int buttonId = Mouse.getEventButton(),
						mouseX = Mouse.getEventX(),
						mouseY = Display.getHeight() - Mouse.getEventY();
				if (Mouse.getEventButtonState())
					current.mousePressed(mouseX, mouseY, buttonId);
				else
					current.mouseReleased(mouseX, mouseY, buttonId);
			}
		}
	}
	
	public void toggleVsync() {
		vsync = !vsync;
		Display.setVSyncEnabled(vsync);
	}
	
	public void toggleFullscreen() {
		fullscreen = !fullscreen;
		try {
			if (fullscreen) {
				Display.setDisplayModeAndFullscreen(Display.getDesktopDisplayMode());
			} else {
				Display.setDisplayMode(new DisplayMode(800, 600));
			}
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
		this.resize();
	}
	
	public static long getTime() {
		return (Sys.getTime() * 1000) / Sys.getTimerResolution();
	}
	
	public void loadScene(Scene scene) {
		if (this.current != null)
			current.close();
		this.current = scene;
	}

	public float getFps() {
		return fps;
	}

	public boolean isVsync() {
		return vsync;
	}

	public boolean isFullscreen() {
		return fullscreen;
	}
	
}
