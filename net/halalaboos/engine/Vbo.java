package net.halalaboos.engine;

import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL11.*;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import net.halalaboos.engine.rendering.GLUtils;

import org.lwjgl.BufferUtils;

public class Vbo {

	private int id;
	
	private final List<Float> points = new ArrayList<Float>();
	
	private int amountOfPoints = 0;
	
	public Vbo() {
		genBuffer();
	}

	public void genBuffer() {
		id = GLUtils.genVbo();
	}
	
	public void addVertex(float x, float y, float z) {
		points.add(x);
		points.add(y);		
		points.add(z);
	}
	
	public void addVertex(double x, double y, double z) {
		points.add((float) x);
		points.add((float) y);		
		points.add((float) z);
	}
	
	public void compile() {
		FloatBuffer floatBuffer = BufferUtils.createFloatBuffer(points.size());
		for (float point : points) {
			floatBuffer.put(point);
		}
		floatBuffer.flip();
		amountOfPoints = points.size() / 3;
		points.clear();
		glBindBuffer(GL_ARRAY_BUFFER, id);
		glBufferData(GL_ARRAY_BUFFER, floatBuffer, GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	
	public void render(int mode) {		
		glBindBuffer(GL_ARRAY_BUFFER, id);
		glVertexPointer(3, GL_FLOAT, 0, 0L);
				
		glEnableClientState(GL_VERTEX_ARRAY);
		glDrawArrays(mode, 0, amountOfPoints);
        glDisableClientState(GL_VERTEX_ARRAY);
		
        glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	
}
