package net.halalaboos.engine;

import net.halalaboos.game.GameScene;

public class Launcher {

	public static void run() {
		Engine.instance.loadScene(new GameScene());
		Engine.instance.run();
	}
	

	public static void main(String[] args) {
		run();
	}
	
}
