package net.halalaboos.engine.utils;

import org.lwjgl.util.vector.Vector2f;

public class SimpleMath {

	public Vector2f getNormal(Vector2f one, Vector2f two, boolean flipped) {
		float dx = two.getX() - one.getX();
		float dy = two.getY() - one.getY();
		return new Vector2f(flipped ? dy : -dy, flipped ? -dx : dx);
	}
	
	public float clampRotation(float rotation) {
		return (rotation % 360);
	}
	
}
