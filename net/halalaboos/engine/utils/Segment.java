package net.halalaboos.engine.utils;

public class Segment {

	public Point a, b;
	
	public Segment(Point a, Point b) {
		this.a = a;
		this.b = b;
	}
	
	public Segment(float x, float y, float x1, float y1) {
		this.a = new Point();
		this.b = new Point();
		this.a.x = x;
		this.a.y = y;
		this.b.x = x1;
		this.b.y = y1;
	}
	
	public float distX() {
		return b.x - a.x;
	}
	
	public float distY() {
		return b.y - a.y;
	}
	
	public float dist() {
		return (float) java.lang.Math.sqrt(distX() * distX() + distY() * distY());
	}
	
	public boolean sameDirection(Segment other) {
		float d = dist(), d1 = other.dist();
		return distX() / d == other.distX() / d1 && distY() / d == other.distY() / d1;
	}
}
