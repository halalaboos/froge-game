package net.halalaboos.engine.api.gui;

import net.halalaboos.engine.api.gui.theme.Theme;

public interface Component {
	
	void update();
	
	int getX();
	
	void setX(int x);
	
	int getY();
	
	void setY(int y);
	
	int getWidth();
	
	void setWidth(int width);
	
	int getHeight();
	
	void setHeight(int height);
	
	void setSize(int width, int height);
	
	boolean isPointInside(int x, int y);
	
	boolean mouseClicked(int x, int y, int buttonId);
	
	void mouseReleased(int x, int y, int buttonId);
	
	void mouseWheel(int amount);

	void keyTyped(int keyCode, char key);
	
	Component getParent();
	
	void setParent(Component component);
		
	int getOffsetX();
	
	void setOffsetX(int x);
	
	int getOffsetY();
	
	void setOffsetY(int y);
	
	boolean hasMouseOver();
	
	void setHasMouseOver(boolean mouseOver);
	
	boolean hasMouseDown();
	
	void setHasMouseDown(boolean mouseDown);
	
	boolean isSelected();
	
	void setSelected(boolean selected);
	
	Theme getTheme();
	
	void setTheme(Theme theme);
		
}
