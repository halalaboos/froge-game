package net.halalaboos.engine.api.gui.containers;

import org.lwjgl.input.Mouse;

import net.halalaboos.engine.api.gui.Component;
import net.halalaboos.engine.api.gui.simple.SimpleContainer;

public class Window extends SimpleContainer {

	protected boolean dragging = false, resizingBottom, resizingRight, resizingLeft, resizingBottomRight, resizingBottomLeft;
	
	protected int tabHeight = 40;
	
	protected int tabBorder = 12;
	
	protected String title;
	
	protected int minWidth = 30, minHeight = 30;
	
	public Window(String title, int x, int y, int width, int height) {
		super(x, y, width, height);
		this.title = title;
	}
	
	@Override
	public void update() {
		if (Mouse.isButtonDown(0)) {
			if (dragging) {
				int dx = Mouse.getDX(), dy = -Mouse.getDY();
				this.setX(this.getX() + dx);
				this.setY(this.getY() + dy);
				this.setOffsetX(this.getOffsetX() + dx);
				this.setOffsetY(this.getOffsetY() + dy);
			} 
			if (this.resizingBottom || this.resizingBottomRight || this.resizingBottomLeft) {
				int dy = -Mouse.getDY();
				this.setHeight(this.getHeight() + dy);
			}
			
			if (this.resizingRight || this.resizingBottomRight) {
				int dx = Mouse.getDX();
				this.setWidth(this.getWidth() + dx);
			}
			
			if (this.resizingLeft || resizingBottomLeft) {
				int dx = Mouse.getDX();
				this.setOffsetX(this.getOffsetX() + dx);
				this.setX(this.getX() + dx);
				this.setWidth(this.getWidth() - dx);
			}
		} else { 
			this.dragging = false;
			this.resizingRight = false;
			this.resizingBottom = false;
			this.resizingLeft = false;
			this.resizingBottomRight = false;
			this.resizingBottomLeft = false;
		}
		if (width < theme.getFontRenderer().getStringWidth(title))
			setWidth(theme.getFontRenderer().getStringWidth(title));
		if (width < minWidth)
			setWidth(minWidth);
		if (height < minHeight)
			setHeight(minHeight);
		super.update();
	}
	
	@Override
	public boolean mouseClicked(int x, int y, int buttonId) {
		if (insideTab(x, y)) {
			dragging = true;
			return true;
		}
		
		int[] resizingBarBottom = this.getResizingBarBottom();
		if (x > resizingBarBottom[0] && x < resizingBarBottom[0] + resizingBarBottom[2] && y > resizingBarBottom[1] && y < resizingBarBottom[1] + resizingBarBottom[3]) {
			resizingBottom = true;
		}
		
		int[] resizingBarRight = this.getResizingBarRight();
		if (x > resizingBarRight[0] && x < resizingBarRight[0] + resizingBarRight[2] && y > resizingBarRight[1] && y < resizingBarRight[1] + resizingBarRight[3]) {
			resizingRight = true;
		}
		
		int[] resizingBarLeft = this.getResizingBarLeft();
		if (x > resizingBarLeft[0] && x < resizingBarLeft[0] + resizingBarLeft[2] && y > resizingBarLeft[1] && y < resizingBarLeft[1] + resizingBarLeft[3]) {
			resizingLeft = true;
		}
		
		int[] resizingBarBottomRight = this.getResizingBarBottomRight();
		if (x > resizingBarBottomRight[0] && x < resizingBarBottomRight[0] + resizingBarBottomRight[2] && y > resizingBarBottomRight[1] && y < resizingBarBottomRight[1] + resizingBarBottomRight[3]) {
			resizingBottomRight = true;
		}
		
		int[] resizingBarBottomLeft = this.getResizingBarBottomLeft();
		if (x > resizingBarBottomLeft[0] && x < resizingBarBottomLeft[0] + resizingBarBottomLeft[2] && y > resizingBarBottomLeft[1] && y < resizingBarBottomLeft[1] + resizingBarBottomLeft[3]) {
			resizingBottomLeft = true;
		}
		return super.mouseClicked(x, y, buttonId);
	}
	
	public int[] getResizingBarBottom() {
		return new int[] { getX(), getY() + this.getHeight(), getWidth(), tabBorder };
	}
	
	public int[] getResizingBarRight() {
		return new int[] { getX() + getWidth(), getY(), tabBorder, getHeight() };
	}
	
	public int[] getResizingBarLeft() {
		return new int[] { getX() - tabBorder, getY(), tabBorder, getHeight() };
	}
	
	public int[] getResizingBarBottomRight() {
		return new int[] { getX() + getWidth(), getY() + getHeight(), tabBorder, tabBorder };
	}
	
	public int[] getResizingBarBottomLeft() {
		return new int[] { getX() - tabBorder, getY() + getHeight(), tabBorder, tabBorder };
	}
	
	@Override
	public boolean isPointInside(int x, int y) {
		for (int i = components.size() - 1; i >= 0; i--) {
			Component component = components.get(i);
			if (component.isPointInside(x, y)) {
				return true;
			}
		}
		return x >= this.x - tabBorder && x <= this.x + this.width + tabBorder && y >= this.y - tabHeight && y <= this.y + this.height + tabBorder;
	}
	
	protected boolean insideTab(int x, int y) {
		return x > this.x - tabBorder && x < this.x + this.width + tabBorder && y > this.y - this.tabHeight && y < this.y;
	}

	public int getTabHeight() {
		return tabHeight;
	}

	public void setTabHeight(int tabHeight) {
		this.tabHeight = tabHeight;
	}

	public boolean isDragging() {
		return dragging;
	}

	public int getTabBorder() {
		return tabBorder;
	}

	public void setTabBorder(int tabBorder) {
		this.tabBorder = tabBorder;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public void pack() {
		this.calculateTotalSize();
		setWidth(totalComponentWidth + 1);
		setHeight(totalComponentHeight + 1);
	}
	
}
