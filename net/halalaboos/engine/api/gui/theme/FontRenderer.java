package net.halalaboos.engine.api.gui.theme;

public interface FontRenderer {
	
	int drawString(String text, int x, int y, int color);
	
	int drawStringWithShadow(String text, int x, int y, int color);
	
	void drawCenteredString(String text, int x, int y, int color);
	
	void drawCenteredStringWithShadow(String text, int x, int y, int color);
	
	int getStringWidth(String text);
	
	int getStringHeight(String text);

	int getShadowColor();
	
	void setShadowColor(int color);
	
}
