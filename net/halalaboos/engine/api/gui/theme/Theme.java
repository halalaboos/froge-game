package net.halalaboos.engine.api.gui.theme;

import net.halalaboos.engine.api.gui.Component;
import net.halalaboos.engine.api.gui.Container;

public interface Theme {

	void render(Container container, Component component);
	
	FontRenderer getFontRenderer();
	
	void setFontRenderer(FontRenderer fontRenderer);
	
	void drawTooltip(String tooltip, float x, float y);
	
}
