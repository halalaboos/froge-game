package net.halalaboos.engine.api.gui;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

import net.halalaboos.engine.api.gui.theme.Theme;
import net.halalaboos.engine.rendering.GLUtils;

public class GuiManager {
	
	private final List<Container> containers = new ArrayList<Container>();
	
	private Theme theme;
	
	private Container selected, down, over;
			
	public void render() {
		int mouseX = Mouse.getX(), mouseY = Display.getHeight() - Mouse.getY();
		calculateMouseOver(mouseX, mouseY);
		for (int i = 0; i < containers.size(); i++) {
			Container container = containers.get(i);
			container.update();
			container.setHasMouseOver(over == container);
			container.setHasMouseDown(down == container);
			container.setSelected(selected == container);
			theme.render(null, container);
			if (container.hasHorizontalScrollbar() || container.hasVerticalScrollbar()) {
				GL11.glEnable(GL11.GL_SCISSOR_TEST);
				GLUtils.glScissor(container.getX(), container.getY(), container.getX() + container.getRenderableWidth(), container.getY() + container.getRenderableHeight());
				renderComponents(container);
				GL11.glDisable(GL11.GL_SCISSOR_TEST);
			} else {
				renderComponents(container);
			}
		}
	}
	
	protected void calculateMouseOver(int mouseX, int mouseY) {
		over = null;
		for (int i = containers.size() - 1; i >= 0; i--) {
			Container container = containers.get(i);
			if (container.isPointInside(mouseX, mouseY)) {
				over = container;
				break;
			}
		}
	}
	
	protected void renderComponents(Container container) {
		List<Component> components = container.getComponents();
		for (int i = 0; i < components.size(); i++) {
			Component component = components.get(i);
			component.update();
			theme.render(container, component);
			if (component instanceof Container) {
				Container internalContainer = (Container) component;
				if (internalContainer.hasHorizontalScrollbar() || internalContainer.hasVerticalScrollbar()) {
					GL11.glEnable(GL11.GL_SCISSOR_TEST);
					GLUtils.glScissor(internalContainer.getX(), internalContainer.getY(), internalContainer.getX() + internalContainer.getRenderableWidth(), internalContainer.getY() + internalContainer.getRenderableHeight());
					renderComponents(internalContainer);
					GL11.glDisable(GL11.GL_SCISSOR_TEST);
				} else {
					renderComponents(internalContainer);
				}
			}
		}
	}
	
	public void mousePressed(int x, int y, int buttonId) {
		for (int i = containers.size() - 1; i >= 0; i--) {
			Container container = containers.get(i);
			if (container.mouseClicked(x, y, buttonId)) {
				selected = container;
				down = container;
				containers.remove(container);
				containers.add(container);
				return;
			}
		}
	}
	
	public void mouseReleased(int x, int y, int buttonId) {
		if (down != null) {
			down.mouseReleased(x, y, buttonId);
			down = null;
		}
	}
	
	public void keyTyped(int keyCode, char key) {
		if (selected != null)
			selected.keyTyped(keyCode, key);
	}
	
	public void mouseWheel(int amount) {
		if (over != null) {
			over.mouseWheel(amount);
		}
	}
	
	public Theme getTheme() {
		return theme;
	}

	public void setTheme(Theme theme) {
		this.theme = theme;
		updateThemes();
	}

	private void updateThemes() {
		for (Container container : containers) {
			container.setTheme(theme);
		}
	}

	public List<Container> getContainers() {
		return containers;
	}

	public void add(Container container) {
		this.containers.add(container);
		container.setTheme(theme);
	}
	
	public void remove(Container container) {
		this.containers.remove(container);
	}
	
}
