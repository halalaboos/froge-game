package net.halalaboos.engine.api.gui;

import java.util.List;

public interface Container extends Component {

	void add(Component component);
	
	void remove(Component component);
	
	void clear();
	
	boolean isLayering();
	
	void setLayering(boolean layering);

	List<Component> getComponents();
	
	boolean isPointInsideRenderable(int x, int y);
	
	int getRenderableX();
	
	int getRenderableY();
	
	int getRenderableWidth();
	
	int getRenderableHeight();
		
	int getTotalComponentHeight();
	
	int getTotalComponentWidth();
	
	int[] getHorizontalScrollbar();
	
	int[] getVerticalScrollbar();

	int getScrollOffsetY();
	
	int getScrollOffsetX();
	
	int getScrollbarSize();
	
	void setScrollbarSize(int size);
	
	int getVerticalScrollbarY();
	
	int getHorizontalScrollbarX();
	
	int getVerticalScrollbarHeight();
	
	int getHorizontalScrollbarWidth();
	
	boolean hasHorizontalScrollbar();
	
	boolean hasVerticalScrollbar();
	
}
