package net.halalaboos.engine.api.gui.simple;

import net.halalaboos.engine.api.gui.Component;
import net.halalaboos.engine.api.gui.theme.Theme;

public class SimpleComponent implements Component {
	
	protected Component parent;
	
	protected int x, y, width, height, offsetX, offsetY;
	
	protected boolean mouseOver, mouseDown, selected;
	
	protected Theme theme;
	
	public SimpleComponent(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.offsetX = x;
		this.offsetY = y;
		this.width = width;
		this.height = height;
	}
	
	@Override
	public int getX() {
		return x;
	}

	@Override
	public void setX(int x) {
		this.x = x;
	}

	@Override
	public int getY() {
		return y;
	}

	@Override
	public void setY(int y) {
		this.y = y;
	}
	
	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public void setWidth(int width) {
		this.width = width;
	}

	@Override
	public int getHeight() {
		return height;
	}

	@Override
	public void setHeight(int height) {
		this.height = height;
	}

	@Override
	public void setSize(int width, int height) {
		this.width = width;
		this.height = height;
	}

	@Override
	public boolean isPointInside(int x, int y) {
		return x >= this.x && x <= this.x + this.width && y >= this.y && y <= this.y + this.height;
	}

	@Override
	public boolean mouseClicked(int x, int y, int buttonId) {
		return isPointInside(x, y);
	}

	@Override
	public void mouseReleased(int x, int y, int buttonId) {
	}

	@Override
	public void keyTyped(int keyCode, char key) {
	}

	@Override
	public Component getParent() {
		return parent;
	}

	@Override
	public void setParent(Component component) {
		this.parent = component;
	}
	
	@Override
	public int getOffsetX() {
		return offsetX;
	}

	@Override
	public int getOffsetY() {
		return offsetY;
	}

	@Override
	public void update() {
	}

	@Override
	public void setOffsetX(int x) {
		this.offsetX = x;
	}

	@Override
	public void setOffsetY(int y) {
		this.offsetY = y;
	}

	@Override
	public void mouseWheel(int amount) {
	}

	@Override
	public boolean hasMouseOver() {
		return mouseOver;
	}

	@Override
	public void setHasMouseOver(boolean mouseOver) {
		this.mouseOver = mouseOver;
	}

	@Override
	public boolean hasMouseDown() {
		return mouseDown;
	}

	@Override
	public void setHasMouseDown(boolean mouseDown) {
		this.mouseDown = mouseDown;
	}

	@Override
	public Theme getTheme() {
		return theme;
	}

	@Override
	public void setTheme(Theme theme) {
		this.theme = theme;
	}

	@Override
	public boolean isSelected() {
		return selected;
	}

	@Override
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

}
