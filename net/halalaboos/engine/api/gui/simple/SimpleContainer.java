package net.halalaboos.engine.api.gui.simple;

import java.util.ArrayList;
import java.util.List;

import net.halalaboos.engine.api.gui.Component;
import net.halalaboos.engine.api.gui.Container;
import net.halalaboos.engine.api.gui.theme.Theme;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

public class SimpleContainer extends SimpleComponent implements Container {

	protected List<Component> components = new ArrayList<Component>();
	
	protected boolean layering = true, draggingHorizontal, draggingVertical;
	
	protected Component selected, down, over;
	
	protected int totalComponentWidth, totalComponentHeight, scrollbarSize = 12;
	
	protected float scrollbarPercentageX, scrollbarPercentageY;
		
	public SimpleContainer(int x, int y, int width, int height) {
		super(x, y, width, height);
		
	}
	
	@Override
	public void add(Component component) {
		component.setParent(this);
		component.setTheme(theme);
		components.add(component);
	}

	@Override
	public void remove(Component component) {
		component.setParent(null);
		component.setTheme(theme);
		components.remove(component);
	}
		
	@Override
	public void clear() {
		components.clear();
	}
	
	@Override
	public boolean mouseClicked(int x, int y, int buttonId) {
		boolean inside = super.mouseClicked(x, y, buttonId);
		selected = down = null;
		int[] horizontalScrollbar = this.getHorizontalScrollbar();
		if (x > horizontalScrollbar[0] && x < horizontalScrollbar[0] + horizontalScrollbar[2] && y > horizontalScrollbar[1] && y < horizontalScrollbar[1] + horizontalScrollbar[3]) {
			draggingHorizontal = true;
			inside = true;
		}
		int[] verticalScrollbar = this.getVerticalScrollbar();
		if (x > verticalScrollbar[0] && x < verticalScrollbar[0] + verticalScrollbar[2] && y > verticalScrollbar[1] && y < verticalScrollbar[1] + verticalScrollbar[3]) {
			draggingVertical = true;
			inside = true;
		}
		for (int i = components.size() - 1; i >= 0; i--) {
			Component component = components.get(i);
			if (component.mouseClicked(x, y, buttonId)) {
				selected = down = component;
				if (layering) {
					components.remove(component);
					components.add(0, component);
				}
				return true;
			}
		}
		return inside;
	}

	@Override
	public void mouseReleased(int x, int y, int buttonId) {
		if (down != null) {
			down.mouseReleased(x, y, buttonId);
			down = null;
		}
	}

	@Override
	public void keyTyped(int keyCode, char key) {
		if (selected != null)
			selected.keyTyped(keyCode, key);
	}
	
	@Override
	public void mouseWheel(int amount) {
		int mouseX = Mouse.getX(), mouseY = Display.getHeight() - Mouse.getY() - 1;
		float xOffset = (1F - (float) (mouseX - x) / (float) width), yOffset = (1F - (float) (mouseY - y) / (float) height);
		if (this.hasHorizontalScrollbar() && this.hasVerticalScrollbar()) {
			if (yOffset < 0.15F)
				this.scrollbarPercentageX += (float) -amount / (((float) getTotalComponentWidth() / (float) getWidth()) * ((float) getTotalComponentWidth() / (float) getWidth()));
			else
				this.scrollbarPercentageY += (float) -amount / (((float) getTotalComponentHeight() / (float) getHeight()) * ((float) getTotalComponentHeight() / (float) getHeight()));
		} else if (this.hasHorizontalScrollbar()) {
			this.scrollbarPercentageX += (float) -amount / (((float) getTotalComponentWidth() / (float) getWidth()) * ((float) getTotalComponentWidth() / (float) getWidth()));
		} else if (this.hasVerticalScrollbar())
			this.scrollbarPercentageY += (float) -amount / (((float) getTotalComponentHeight() / (float) getHeight()) * ((float) getTotalComponentHeight() / (float) getHeight()));
		else {
			if (this.over != null)
				this.over.mouseWheel(amount);
		}
		this.keepSafe();	
		
	}
	
	@Override
	public boolean isPointInside(int x, int y) {
		for (int i = components.size() - 1; i >= 0; i--) {
			Component component = components.get(i);
			if (component.isPointInside(x, y)) {
				return true;
			}
		}
		return super.isPointInside(x, y);
	}
	
	@Override
	public void update() {
		if (!this.hasHorizontalScrollbar())
			this.scrollbarPercentageX = 0;
		if (!this.hasVerticalScrollbar())
			this.scrollbarPercentageY = 0;
		calculateTotalSize();
		calculateMouseOver(Mouse.getX(), Display.getHeight() - Mouse.getY() - 1);
		if (this.draggingHorizontal && Mouse.isButtonDown(0)) {
			this.scrollbarPercentageX += (float) Mouse.getDX() / (float) (getHorizontalScrollbar()[2] - getHorizontalScrollbarWidth());
		} else
			this.draggingHorizontal = false;
		if (this.draggingVertical && Mouse.isButtonDown(0)) {
			this.scrollbarPercentageY += (float) -Mouse.getDY() / (float) (getVerticalScrollbar()[3] - getVerticalScrollbarHeight());
		} else
			this.draggingVertical = false;
		keepSafe();
	}
	
	protected void calculateMouseOver(int mouseX, int mouseY) {
		over = null;
		for (int i = components.size() - 1; i >= 0; i--) {
			Component component = components.get(i);
			component.setHasMouseDown(mouseDown && down == component);
			component.setSelected(this.isSelected() && this.selected == component);
			component.setX(this.getX() + component.getOffsetX() - getScrollOffsetX());
			component.setY(this.getY() + component.getOffsetY() - getScrollOffsetY());
			if (over == null && component.isPointInside(mouseX, mouseY)) {
				over = component;
				over.setHasMouseOver(this.mouseOver);
			} else {
				component.setHasMouseOver(false);
			}
		}
	}
	
	protected void calculateTotalSize() {
		totalComponentWidth = 0;
		totalComponentHeight = 0;
		for (int i = 0; i < components.size(); i++) {
			Component component = components.get(i);
			if (component.getOffsetX() + component.getWidth() > totalComponentWidth) {
				totalComponentWidth = component.getOffsetX() + component.getWidth();
			}
			if (component.getOffsetY() + component.getHeight() > totalComponentHeight) {
				totalComponentHeight = component.getOffsetY() + component.getHeight();
			}
		}
	}
	
	@Override
	public int getVerticalScrollbarHeight() {
		float ratio = (float) getRenderableHeight() / (float) (this.totalComponentHeight);
		if (ratio * getRenderableHeight() < 12)
			return 12;
		else
			return (int) (ratio * getRenderableHeight());
	}
	
	@Override
	public int getHorizontalScrollbarWidth() {
		float ratio = (float) getRenderableWidth() / (float) (this.totalComponentWidth);
		if (ratio * getRenderableWidth() < 12)
			return 12;
		else
			return (int) (ratio * getRenderableWidth());
	}
	
	protected void keepSafe() {
		if (scrollbarPercentageX > 1)
			scrollbarPercentageX = 1;
		if (scrollbarPercentageX < 0)
			scrollbarPercentageX = 0;
		if (scrollbarPercentageY > 1)
			scrollbarPercentageY = 1;
		if (scrollbarPercentageY < 0)
			scrollbarPercentageY = 0;
	}
	
	@Override
	public int getScrollOffsetY() {
		return hasVerticalScrollbar() ? (int) ((this.totalComponentHeight - getRenderableHeight()) * scrollbarPercentageY) : 0;
	}
	
	@Override
	public int getScrollOffsetX() {
		return hasHorizontalScrollbar() ? (int) ((this.totalComponentWidth - getRenderableWidth()) * scrollbarPercentageX) : 0;
	}

	@Override
	public boolean isLayering() {
		return layering;
	}

	@Override
	public void setLayering(boolean layering) {
		this.layering = layering;
	}
	
	@Override
	public List<Component> getComponents() {
		return components;
	}

	@Override
	public int getTotalComponentHeight() {
		return totalComponentHeight;
	}

	@Override
	public int getTotalComponentWidth() {
		return totalComponentWidth;
	}

	@Override
	public int[] getHorizontalScrollbar() {
		return new int[] { getX(), getY() + this.getHeight() - scrollbarSize, this.getWidth(), scrollbarSize };
	}

	@Override
	public int[] getVerticalScrollbar() {
		return new int[] { getX() + this.getWidth() - scrollbarSize, getY(), scrollbarSize, this.getHeight() };
	}

	@Override
	public int getVerticalScrollbarY() {
		return (int) (scrollbarPercentageY *  getHeightForVertical());
	}
	
	protected float getHeightForVertical() {
		int SLIDER_PADDING = 0;
		float maxPointForRendering = (float) (getHeight() - getVerticalScrollbarHeight() - SLIDER_PADDING),
				beginPoint = (SLIDER_PADDING);
		return maxPointForRendering - beginPoint;
	}

	@Override
	public int getHorizontalScrollbarX() {
		return (int) (scrollbarPercentageX *  getWidthForHorizontal());
	}
	
	protected float getWidthForHorizontal() {
		int SLIDER_PADDING = 0;
		float maxPointForRendering = (float) (getWidth() - getHorizontalScrollbarWidth() - SLIDER_PADDING),
				beginPoint = (SLIDER_PADDING);
		return maxPointForRendering - beginPoint;
	}

	@Override
	public boolean hasHorizontalScrollbar() {
		return width < this.getTotalComponentWidth();
	}

	@Override
	public boolean hasVerticalScrollbar() {
		return height < this.getTotalComponentHeight();
	}

	@Override
	public int getScrollbarSize() {
		return scrollbarSize;
	}

	@Override
	public void setScrollbarSize(int size) {
		this.scrollbarSize = size;
	}

	@Override
	public int getRenderableWidth() {
		return this.hasVerticalScrollbar() ? width - scrollbarSize : width;
	}

	@Override
	public int getRenderableHeight() {
		return this.hasHorizontalScrollbar() ? height - scrollbarSize : height;
	}

	@Override
	public boolean isPointInsideRenderable(int x, int y) {
		return x >= this.getRenderableX() && x <= this.getRenderableX() + this.getRenderableWidth() && y >= this.getRenderableY() && y <= this.getRenderableY() + this.getRenderableHeight();
	}

	@Override
	public int getRenderableX() {
		return x;
	}

	@Override
	public int getRenderableY() {
		return y;
	}
	
	@Override
	public void setTheme(Theme theme) {
		super.setTheme(theme);
		updateThemes();
	}

	private void updateThemes() {
		for (Component component : components) {
			component.setTheme(theme);
		}
	}

}
