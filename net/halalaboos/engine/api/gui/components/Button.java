package net.halalaboos.engine.api.gui.components;

import net.halalaboos.engine.api.gui.simple.SimpleComponent;

public class Button extends SimpleComponent {
	
	private String title;
	
	public Button(String title, int x, int y, int width, int height) {
		super(x, y, width, height);
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
}
