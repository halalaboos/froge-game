package net.halalaboos.engine.api.gui.components;

import net.halalaboos.engine.api.gui.simple.SimpleComponent;

public class Label extends SimpleComponent {

	private String text;
	
	public Label(String text, int x, int y) {
		super(x, y, 0, 0);
		this.text = text;
	}
	
	@Override
	public void update() {
		super.update();
		if (theme != null) {
			this.width = theme.getFontRenderer().getStringWidth(text) + 2;
			this.height = theme.getFontRenderer().getStringHeight(text);
		}
	}
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
