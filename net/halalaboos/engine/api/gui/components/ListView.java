package net.halalaboos.engine.api.gui.components;

import java.util.ArrayList;
import java.util.List;

import net.halalaboos.engine.api.gui.simple.SimpleComponent;

public class ListView <O> extends SimpleComponent {

	protected List<O> items = new ArrayList<O>();
		
	public ListView(int x, int y) {
		super(x, y, 0, 0);
	}
	
	@Override
	public void update() {
		super.update();
		if (theme != null) {
			int width = 0, height = 0;
			for (O object : items) {
				String toString = object.toString();
				int objectWidth = theme.getFontRenderer().getStringWidth(toString) + 2;
				if (width < objectWidth)
					width = objectWidth;
				height += theme.getFontRenderer().getStringHeight(toString);
			}
			this.width = width;
			this.height = height;
		}
	}

	public void add(O object) {
		items.add(object);
	}

	public void remove(O object) {
		items.remove(object);
	}

	public List<O> getItems() {
		return items;
	}
}
