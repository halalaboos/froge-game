package net.halalaboos.engine.api.gui.components;

import java.util.ArrayList;
import java.util.List;

import net.halalaboos.engine.api.gui.simple.SimpleComponent;

public class Dropdown <O> extends SimpleComponent {
	
	protected List<O> items = new ArrayList<O>();
	
	protected boolean down;
	
	protected int selected = -1;
	
	protected int padding = 1, itemPadding = 1;
	
	public Dropdown(int x, int y, int width, int height) {
		super(x, y, width, height);
		
	}
	

	@Override
	public void update() {
		super.update();
		if (!this.isSelected())
			down = false;
	}
	
	@Override
	public boolean mouseClicked(int x, int y, int buttonId) {
		if (super.isPointInside(x, y))
			down = !down;
		if (down) {
			int[] dropdown = this.getDropdown();
			if (x > dropdown[0] && x < dropdown[0] + dropdown[2] && y > dropdown[1] && y < dropdown[1] + dropdown[3]) {
				return true;
			}
		}
		return super.mouseClicked(x, y, buttonId);
	}
	
	@Override
	public boolean isPointInside(int x, int y) {
		int[] dropdown = this.getDropdown();
		return super.isPointInside(x, y) || (down ? (x > dropdown[0] && x < dropdown[0] + dropdown[2] && y > dropdown[1] && y < dropdown[1] + dropdown[3]) : false);	
	}

	@Override
	public void mouseReleased(int x, int y, int buttonId) {
		super.mouseReleased(x, y, buttonId);
		if (down) {
			int[] dropdown = this.getDropdown();
			if (x > dropdown[0] && x < dropdown[0] + dropdown[2] && y > dropdown[1] && y < dropdown[1] + dropdown[3]) {
				int height = 0;
				for (int i = 0; i < items.size(); i++) {
					if (x > dropdown[0] && x < dropdown[0] + dropdown[2] && y > dropdown[1] + height && y < dropdown[1] + height + getItemSize()) {
						this.selected = i;
						break;
					}
					height += getItemSize() + itemPadding;
				}
			}
		}
	}
	
	public int[] getDropdown() {
		return new int[] { getX(), getY() + this.getHeight() + padding, this.getWidth(), getDropdownHeight() };
	}
	
	private int getDropdownHeight() {
		return items.size() * (getItemSize() + itemPadding) + padding;
	}

	public void add(O item) {
		items.add(item);
	}
	
	public void remove(O item) {
		items.remove(item);
	}
	
	public List<O> getItems() {
		return items;
	}
	
	public void setItems(List<O> items) {
		this.items = items;
	}
	
	public int getSelected() {
		return selected;
	}
	
	public void setSelected(int selected) {
		this.selected = selected;
	}

	public boolean isDown() {
		return down;
	}

	public void setDown(boolean down) {
		this.down = down;
	}
	
	public int getItemSize() {
		return 18;
	}

	public int getPadding() {
		return padding;
	}

	public void setPadding(int padding) {
		this.padding = padding;
	}

	public int getItemPadding() {
		return itemPadding;
	}

	public void setItemPadding(int itemPadding) {
		this.itemPadding = itemPadding;
	}
	
	public O getSelectedItem() {
		return selected >= 0 && selected < items.size() ? items.get(selected) : null;
	}
}
