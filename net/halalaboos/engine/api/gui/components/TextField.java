package net.halalaboos.engine.api.gui.components;

import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;

import net.halalaboos.engine.api.gui.simple.SimpleComponent;
import net.halalaboos.engine.utils.Timer;

public class TextField extends SimpleComponent {
	
	protected final Timer timer = new Timer();
	
	protected String text = "";
		
	protected String carot = "_";

	protected int maxLength = 100;
	
//	protected int highlightIndex = 0;
	
	public TextField(int x, int y, int width, int height) {
		super(x, y, width, height);
		
	}
	
	@Override
	public void keyTyped(int keyCode, char key) {
		super.keyTyped(keyCode, key);
		Keyboard.enableRepeatEvents(true);
//		if (keyCode == Keyboard.KEY_LEFT) {
//			highlightIndex--;
//		}
//		if (keyCode == Keyboard.KEY_RIGHT) {
//			highlightIndex++;
//		}
//		if (highlightIndex < 0)
//			highlightIndex = 0;
//		if (highlightIndex > text.length())
//			highlightIndex = text.length();
		
		if (keyCode == Keyboard.KEY_V) {
			if (Keyboard.isKeyDown(Keyboard.KEY_LCONTROL) || Keyboard.isKeyDown(Keyboard.KEY_RCONTROL)) {
				if (Sys.getClipboard() != null) {
					text += Sys.getClipboard();
				}
			}
		}
		if (keyCode == Keyboard.KEY_BACK) {
				minus(1);
		} else if (isAllowedCharacter(key))
			text += key;
		if (text.length() > maxLength && maxLength > 0)
			text = text.substring(0, maxLength);
	}
	
	private void minus(int ammount) {
		if ((text.length() - ammount) >= 0)
			text = text.substring(0, text.length() - ammount);
	}

	protected boolean isAllowedCharacter(char character) {
		return character != 167 && character >= 32 && character != 127;
	}
	
	public String getCarot() {
		if (timer.hasReach(250)) {
			if (carot.equals("_"))
                carot = "";
            else
                carot = "_";
			timer.reset();
		}
        return selected ? carot : "";
	}

	public String getTextForRender(float width) {
        if (theme.getFontRenderer().getStringWidth(text + "_") > width) {
            String text = "";
            char[] chars = this.text.toCharArray();
            for (int index = chars.length - 1; index >= 0; index--) {
                if (theme.getFontRenderer().getStringWidth(chars[index] + text + "_") >= width)
                    break;
                text = chars[index] + text;
            }
            return text;
        }
        return text;
    }
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean hasText() {
		return !text.isEmpty();
	}
	
	public void clear() {
		text = "";
	}

}
