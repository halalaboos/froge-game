package net.halalaboos.engine.rendering;

import static org.lwjgl.opengl.GL11.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import net.halalaboos.engine.utils.Segment;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Vector2f;
import org.newdawn.slick.opengl.PNGDecoder;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public final class GLUtils {
	
	public static List<Texture> textures = new ArrayList<Texture>();
	
	public static List<Integer> textureIds = new ArrayList<Integer>(),
			vaos = new ArrayList<Integer>(),
			vbos = new ArrayList<Integer>();;
	
	private GLUtils() {
		
	}
	
	public static void glScissor(float x, float y, float x1, float y1) {
		int factor = 1;
		GL11.glScissor((int) (x * factor), (int) (Display.getHeight() - (y1 * factor)), (int) ((x1 - x) * factor), (int) ((y1 - y) * factor));
	}
	
	public static int genTexture() {
		int textureId = GL11.glGenTextures();
		textureIds.add(textureId);
		return textureId;
	}
	
	/**
	 * @param filter determines how the texture will interpolate when scaling up / down. <br>
	 * GL_LINEAR - smoothest <br> GL_NEAREST - most accurate <br>
	 * @param wrap determines how the UV coordinates outside of the 0.0F ~ 1.0F range will be handled. <br>
	 * GL_CLAMP_TO_EDGE - samples edge color <br> GL_REPEAT - repeats the texture <br>
	 * */
	public static int applyTexture(int texId, File file, int filter, int wrap) throws IOException {
		applyTexture(texId, ImageIO.read(file), filter, wrap);
        return texId;
	}
	
	/**
	 * @param filter determines how the texture will interpolate when scaling up / down. <br>
	 * GL_LINEAR - smoothest <br> GL_NEAREST - most accurate <br>
	 * @param wrap determines how the UV coordinates outside of the 0.0F ~ 1.0F range will be handled. <br>
	 * GL_CLAMP_TO_EDGE - samples edge color <br> GL_REPEAT - repeats the texture <br>
	 * */
	public static int applyTexture(int texId, BufferedImage image, int filter, int wrap) {
		int[] pixels = new int[image.getWidth() * image.getHeight()];
        image.getRGB(0, 0, image.getWidth(), image.getHeight(), pixels, 0, image.getWidth());

        ByteBuffer buffer = BufferUtils.createByteBuffer(image.getWidth() * image.getHeight() * 4);
        
        for(int y = 0; y < image.getHeight(); y++){
            for(int x = 0; x < image.getWidth(); x++){
                int pixel = pixels[y * image.getWidth() + x];
                buffer.put((byte) ((pixel >> 16) & 0xFF));
                buffer.put((byte) ((pixel >> 8) & 0xFF));
                buffer.put((byte) (pixel & 0xFF));
                buffer.put((byte) ((pixel >> 24) & 0xFF));
            }
        }

        buffer.flip();
        applyTexture(texId, image.getWidth(), image.getHeight(), buffer, filter, wrap);
        return texId;
	}
	
	/**
	 * @param filter determines how the texture will interpolate when scaling up / down. <br>
	 * GL_LINEAR - smoothest <br> GL_NEAREST - most accurate <br>
	 * @param wrap determines how the UV coordinates outside of the 0.0F ~ 1.0F range will be handled. <br>
	 * GL_CLAMP_TO_EDGE - samples edge color <br> GL_REPEAT - repeats the texture <br>
	 * */
	public static int applyTexture(int texId, int width, int height, ByteBuffer pixels, int filter, int wrap) {
		glBindTexture(GL_TEXTURE_2D, texId);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
		glBindTexture(GL_TEXTURE_2D, 0);
        return texId;
	}
	
	public static Texture loadTexture(String name) {
		try {
			Texture texture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/" + name));
			textures.add(texture);
			return texture;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static int genVao() {
		int id = GL30.glGenVertexArrays();
		vaos.add(id);
		GL30.glBindVertexArray(id);
		return id;
	}
	
	public static int genVbo() {
		int id = GL15.glGenBuffers();
		vbos.add(id);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, id);
		return id;
	}
	
	public static void storeAttribute(int attribute, int size, FloatBuffer data) {
		int vboId = GL15.glGenBuffers();
		vbos.add(vboId);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboId);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, data, GL15.GL_STATIC_DRAW);
		GL20.glVertexAttribPointer(attribute, size, GL11.GL_FLOAT, false, 0, 0);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
	}
	
	public static ByteBuffer loadIcon(String path) throws IOException {
		PNGDecoder decoder = new PNGDecoder(ResourceLoader.getResourceAsStream(path));
		ByteBuffer bytebuf = ByteBuffer.allocateDirect(decoder.getWidth() * decoder.getHeight() * 4);
		decoder.decode(bytebuf, decoder.getWidth() * 4, PNGDecoder.RGBA);
		bytebuf.flip();
		return bytebuf;
    }
	
	public static void drawBorder(float x, float y, float x1, float y1, float size) {
		drawRect(x - size, y, x, y1 + size); // left side
		drawRect(x1, y, x1 + size, y1 + size); // right side
		drawRect(x, y1, x1, y1 + size); // bottom
	}
	
	public static void drawRect(float x, float y, float x1, float y1) {
		glBegin(GL_TRIANGLES);
		glVertex2f(x1, y1);
		glVertex2f(x1, y);
		glVertex2f(x, y);
		glVertex2f(x, y);
		glVertex2f(x, y1);
		glVertex2f(x1, y1);
		glEnd();
	}
	
	public static void drawTexturedRect(float x, float y, float width, float height, float u, float v, float t, float s) {

		glBegin(GL_TRIANGLES);
		
		glTexCoord2f(u, v);
		glVertex2f(x, y);
		
		glTexCoord2f(t, v);
		glVertex2f(x + width, y);
		
		glTexCoord2f(u, s);
		glVertex2f(x, y + height);
		
		glTexCoord2f(t, v);
		glVertex2f(x + width, y);
		
		glTexCoord2f(t, s);
		glVertex2f(x + width, y + height);
		
		glTexCoord2f(u, s);
		glVertex2f(x, y + height);
		
		glEnd();
	
	}
	
	public static void drawLine(float x, float y, float x1, float y1) {
		glBegin(GL_LINES);
		glVertex2f(x, y);
		glVertex2f(x1, y1);
		glEnd();
	}
	
	public static void drawPolygon(List<Vector2f> vertices, List<Vector2f> normals, float rotation, float x, float y) {
		glPushMatrix();
		glTranslatef(x, y, 0F);
		glBegin(GL_LINE_LOOP);
		glColor3f(1F, 1F, 1F);
		for (Vector2f vertex : vertices) {
			glVertex2f(vertex.x, vertex.y);
		}
		glEnd();
		glBegin(GL_LINES);
		for (int i = 0; i < normals.size(); ++i) {
			Vector2f normal = normals.get(i);
			float distance = 50F;
			glColor3f(1F, 0F, 1F);
			glVertex2f(0, 0);
			glColor3f(0F, 0F, 1F);
			glVertex2f(normal.x * distance, normal.y * distance);
		}
		glEnd();
		glPopMatrix();
	}
	
	public static void drawSegments(List<Segment> segments, float x, float y) {
		glPushMatrix();
		glTranslatef(x, y, 0);
		for (Segment segment : segments) {
			drawSegment(segment);
		}
		glPopMatrix();
	}
	
	public static void drawSegment(Segment segment) {
		glBegin(GL_LINES);
		glVertex2f(segment.a.x, segment.a.y);
		glVertex2f(segment.b.x, segment.b.y);
		glEnd();
	}
	
	public static void setupMatrix(int width, int height) {
		glViewport(0, 0, width, height);
	}
	
	public static void cleanup() {
		GL30.glBindVertexArray(0);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
		for (Texture texture : textures) {
			texture.release();
		}
		for (Integer texId : textureIds) {
			GL11.glDeleteTextures(texId);
		}
		for (int vao : vaos) {
			GL30.glDeleteVertexArrays(vao);
		}
		for (int vbo : vbos) {
			GL15.glDeleteBuffers(vbo);
		}
	}
	
}
