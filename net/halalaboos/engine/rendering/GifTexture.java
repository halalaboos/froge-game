package net.halalaboos.engine.rendering;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.metadata.IIOMetadataNode;

import net.halalaboos.engine.utils.Timer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.newdawn.slick.util.ResourceLoader;

import static org.lwjgl.opengl.GL11.*;

public class GifTexture {
		
	private final Timer timer = new Timer();
	
	private final List<ImageData> frames = new ArrayList<ImageData>();
	
	private int width, height;
	
	private int pass = 0;
	
	private int texId;
			
	public GifTexture(String textureName) {
		try {
			loadFrames(textureName);
		} catch (IOException e) {
			e.printStackTrace();
		}
		texId = GLUtils.genTexture();
	}
	
	public void update(float delta) {
		if (timer.hasReach(getImageData().delayTime)) {
			pass++;
			if (pass >= frames.size())
				pass = 0;
			updateTexture();
			timer.reset();
		}
	}

	private ImageData getImageData() {
		return frames.get(pass);
	}
	
	private void updateTexture() {
		ImageData image = getImageData();
		
		System.out.println("pass=" + pass + ", delayTimeMS=" + image.delayTime + ", delayTimeBrowser=" + (image.delayTime / 10));
		glBindTexture(GL_TEXTURE_2D, texId);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, image.width, image.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image.buffer);
		glBindTexture(GL_TEXTURE_2D, 0);

	}

	private void loadFrames(String name) throws IOException {
		ImageReader reader = ImageIO.getImageReadersBySuffix("gif").next();
		reader.setInput(ImageIO.createImageInputStream(ResourceLoader.getResourceAsStream("res/" + name)));
		int min = reader.getMinIndex(), numImages = reader.getNumImages(true);
		BufferedImage oldImage = null;
		for (int i = min; i < numImages; i++) {
			if (i != min && oldImage != null) {
				oldImage.getGraphics().drawImage(reader.read(i), 0, 0, null);
			} else {
				oldImage = reader.read(i);
			}
			BufferedImage newImage = oldImage;
			ImageData imageData = new ImageData();
			
			if (newImage.getWidth() > width)
				width = newImage.getWidth();
			if (newImage.getHeight() > height)
				height = newImage.getHeight();
			
			imageData.width = newImage.getWidth();
			imageData.height = newImage.getHeight();
			imageData.delayTime = getDelayTime(reader, i);
			imageData.buffer = BufferUtils.createByteBuffer(imageData.width * imageData.height * 4);
			int[] pixels = new int[imageData.width * imageData.height];
			newImage.getRGB(0, 0, imageData.width, imageData.height, pixels, 0, imageData.width);
			
				for (int y = 0; y < imageData.height; y++) {
					for (int x = 0; x < imageData.width; x++) {
						int pixel = pixels[y * imageData.width + x];
						imageData.buffer.put((byte) ((pixel >> 16) & 0xFF)); // Red component
						imageData.buffer.put((byte) ((pixel >> 8) & 0xFF)); // Green component
						imageData.buffer.put((byte) (pixel & 0xFF)); // Blue component
						imageData.buffer.put((byte) ((pixel >> 24) & 0xFF)); // Alpha component.
					}
				}
			imageData.buffer.flip();
			frames.add(imageData);
		}
	}
	
	private int getDelayTime(ImageReader reader, int index) throws IOException {
		IIOMetadata imageMetaData = reader.getImageMetadata(index);
		String metaFormatName = imageMetaData.getNativeMetadataFormatName();
		IIOMetadataNode root = (IIOMetadataNode) imageMetaData.getAsTree(metaFormatName);
		IIOMetadataNode graphicsControlExtensionNode = getNode(root, "GraphicControlExtension");
		return Integer.parseInt(graphicsControlExtensionNode.getAttribute("delayTime")) * 10;
	}

	private static IIOMetadataNode getNode(IIOMetadataNode rootNode, String nodeName) {
		int nNodes = rootNode.getLength();
		for (int i = 0; i < nNodes; i++) {
			if (rootNode.item(i).getNodeName().compareToIgnoreCase(nodeName) == 0) {
				return ((IIOMetadataNode) rootNode.item(i));
			}
		}
		IIOMetadataNode node = new IIOMetadataNode(nodeName);
		rootNode.appendChild(node);
		return (node);
	}

	public int getPass() {
		return pass;
	}

	public void setPass(int pass) {
		this.pass = pass;
	}

	public void render(float x, float y, float width, float height) {
		GL11.glColor3f(1F, 1F, 1F);
		glEnable(GL_TEXTURE_2D);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, texId);
		GLUtils.drawTexturedRect(x, y, width, height, 0, 0, 1, 1);
		glDisable(GL_TEXTURE_2D);

	}

	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	private class ImageData {
		ByteBuffer buffer;
		int width, height, delayTime;
	}
	
}
