package net.halalaboos.engine.rendering;

import java.nio.ByteBuffer;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL32;

public class Framebuffer {

	private final int frameBuffer;

	private int textureId;

	private int depthBuffer;

	private int width, height;
	
	public Framebuffer(int width, int height) {
		this.width = width;
		this.height = height;
		frameBuffer = createFrameBuffer();
		textureId = createTextureAttachment(width, height);
		depthBuffer = createDepthBufferAttachment(width, height);
		unbind();
	}

	public void dispose() {
		GL30.glDeleteFramebuffers(frameBuffer);
		GL11.glDeleteTextures(textureId);
		GL30.glDeleteRenderbuffers(depthBuffer);
	}
	
	public void unbind() {
		GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, 0);
		GLUtils.setupMatrix(Display.getWidth(), Display.getHeight());
	}

	public int getTextureId() {
		return textureId;
	}
	
	public int getFrameBufferId() {
		return frameBuffer;
	}
	
	public void bind() {
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
		GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, frameBuffer);
		GLUtils.setupMatrix(width, height);
	}

	private int createFrameBuffer() {
		int frameBuffer = GL30.glGenFramebuffers();
		GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, frameBuffer);
		GL11.glDrawBuffer(GL30.GL_COLOR_ATTACHMENT0);
		return frameBuffer;
	}

	private int createTextureAttachment(int width, int height) {
		int id = GL11.glGenTextures();
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, id);
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB, width, height, 0, GL11.GL_RGB, GL11.GL_UNSIGNED_BYTE, (ByteBuffer) null);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL32.glFramebufferTexture(GL30.GL_FRAMEBUFFER, GL30.GL_COLOR_ATTACHMENT0, id, 0);
		return id;
	}

	private int createDepthBufferAttachment(int width, int height) {
		int depthBuffer = GL30.glGenRenderbuffers();
		GL30.glBindRenderbuffer(GL30.GL_RENDERBUFFER, depthBuffer);
		GL30.glRenderbufferStorage(GL30.GL_RENDERBUFFER, GL11.GL_DEPTH_COMPONENT, width, height);
		GL30.glFramebufferRenderbuffer(GL30.GL_FRAMEBUFFER, GL30.GL_DEPTH_ATTACHMENT, GL30.GL_RENDERBUFFER, depthBuffer);
		return depthBuffer;
	}
	
	public void resize(int width, int height) {
		this.width = width;
		this.height = height;
		GL11.glDeleteTextures(textureId);
		GL30.glDeleteRenderbuffers(depthBuffer);
		bind();
		textureId = createTextureAttachment(width, height);
		depthBuffer = createDepthBufferAttachment(width, height);
		unbind();
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
	public void render(float x, float y, float width, float height) {
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, getTextureId());
		GLUtils.drawTexturedRect(x, y, width, height, 0F, 1F, 1F, 0F);
		glDisable(GL_TEXTURE_2D);
	}
}

