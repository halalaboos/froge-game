package net.halalaboos.engine;

public interface Scene {

	void setup(Engine engine);
	
	void render(Vbo vbo);
	
	void resize(int width, int height);
	
	void keyTyped(int keyCode, char key);
	
	void mousePressed(int x, int y, int buttonId);
	
	void mouseReleased(int x, int y, int buttonId);

	void mouseWheel(int amount);
	
	void update(float delta);

	void close();
	
}
