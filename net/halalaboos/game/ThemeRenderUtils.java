package net.halalaboos.game;

import static org.lwjgl.opengl.GL11.*;

import java.awt.Color;

import net.halalaboos.engine.api.gui.Container;
import net.halalaboos.engine.api.gui.containers.Window;

public final class ThemeRenderUtils {
	
	private static final Color inside = new Color(70, 70, 70, 151),
			border = new Color(93, 93, 93, 151);
	
	private ThemeRenderUtils() {
		
	}
	
//	public static void drawBorder(Window window) {
//		int x = window.getX(), y = window.getY(), x1 = window.getX() + window.getWidth(), y1 = window.getY() + window.getHeight(),
//				size = window.getTabBorder(), tabHeight = window.getTabHeight();
//		drawRect(x - size, y - tabHeight, x1 + size, y); // top tab
//		drawRect(x - size, y, x, y1 + size); // left side
//		drawRect(x1, y, x1 + size, y1 + size); // right side
//		drawRect(x, y1, x1, y1 + size); // bottom
//	}
	
	public static void drawBorder2(Window window) {
		int x = window.getX(), y = window.getY(), x1 = window.getX() + window.getWidth(), y1 = window.getY() + window.getHeight(),
				size = window.getTabBorder(), tabHeight = window.getTabHeight();
		glColor(border.getRGB());
		drawBorder(2.0F, x - size, y - tabHeight, x1 + size, y1 + size);
		drawBorder(1.0F, x + 1, y, x1, y1);
		
		glColor(inside.getRGB());
		drawRect(x - size, y - tabHeight, x1 + size, y1 + size);
	}
	
	public static void drawScrollbars(Container container) {
		if (container.hasHorizontalScrollbar()) {
			glColor3f(1F, 1F, 0F);
			int[] horizontalScrollbar = container.getHorizontalScrollbar();
			
			
			glColor(inside.getRGB());
			drawRect(horizontalScrollbar[0], 
					horizontalScrollbar[1], 
					horizontalScrollbar[0] + horizontalScrollbar[2], 
					horizontalScrollbar[1]  + horizontalScrollbar[3]);
			drawRect(horizontalScrollbar[0] + container.getHorizontalScrollbarX(), 
					horizontalScrollbar[1], 
					horizontalScrollbar[0] + container.getHorizontalScrollbarX() + container.getHorizontalScrollbarWidth(), 
					horizontalScrollbar[1] + horizontalScrollbar[3]);
		}
		if (container.hasVerticalScrollbar()) {
			int[] verticalScrollbar = container.getVerticalScrollbar();
			
			
			glColor(inside.getRGB());
			drawRect(verticalScrollbar[0], 
					verticalScrollbar[1], 
					verticalScrollbar[0] + verticalScrollbar[2], 
					verticalScrollbar[1] + verticalScrollbar[3]);
			drawRect(verticalScrollbar[0], 
					verticalScrollbar[1] + container.getVerticalScrollbarY(), 
					verticalScrollbar[0] + verticalScrollbar[2], 
					verticalScrollbar[1] + container.getVerticalScrollbarY() + container.getVerticalScrollbarHeight());
		}
	}
	
	public static void drawBorder(float size, float x, float y, float x1, float y1) {
		glLineWidth(size);
		glBegin(GL_LINES);
		glVertex2d(x, y);
		glVertex2d(x, y1);
		glVertex2d(x1, y1);
		glVertex2d(x1, y);
		glVertex2d(x, y);
		glVertex2d(x1, y);
		glVertex2d(x, y1);
		glVertex2d(x1, y1);
		glEnd();
	}
	
	public static void drawRect(float x, float y, float x1, float y1) {
		glBegin(GL_TRIANGLES);
		glVertex2f(x1, y1);
		glVertex2f(x1, y);
		glVertex2f(x, y);
		glVertex2f(x, y);
		glVertex2f(x, y1);
		glVertex2f(x1, y1);
		glEnd();
	}

	public static int getColorWithAffects(int color, boolean mouseOver, boolean mouseDown) {
    	return mouseOver ? (mouseDown ? darker(color, 0.2F) : brighter(color, 0.2F)) : color;
    }
	
	public static int darker(int color, float scale) {
		int red = (color >> 16 & 255), green =  (color >> 8 & 255), blue = (color & 255), alpha = (color >> 24 & 0xff);
		red = (int) (red - red * scale);
		red = Math.min(red, 255);
		green = (int) (green - green * scale);
		green = Math.min(green, 255);
		blue = (int) (blue - blue * scale);
		blue = Math.min(blue, 255);
		return (alpha << 24) | (red << 16) | (green << 8) | blue;
	}
	
	public static int brighter(int color, float scale) {
		int red = (color >> 16 & 255), green =  (color >> 8 & 255), blue = (color & 255), alpha = (color >> 24 & 0xff);
		red = (int) (red + red * scale);
		red = Math.min(red, 255);
		green = (int) (green + green * scale);
		green = Math.min(green, 255);
		blue = (int) (blue + blue * scale);
		blue = Math.min(blue, 255);
		return (alpha << 24) | (red << 16) | (green << 8) | blue;
	}
	
	public static void glColor(int color) {
		glColor4f((float) (color >> 16 & 255) / 255F, (float) (color >> 8 & 255) / 255F, (float) (color & 255) / 255F, (color >> 24 & 0xff) / 255F);
	}
}
