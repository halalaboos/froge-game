package net.halalaboos.game;

import static org.lwjgl.opengl.GL11.*;
import net.halalaboos.engine.api.gui.Component;
import net.halalaboos.engine.api.gui.Container;
import net.halalaboos.engine.api.gui.components.Button;
import net.halalaboos.engine.api.gui.components.Dropdown;
import net.halalaboos.engine.api.gui.components.Label;
import net.halalaboos.engine.api.gui.components.ListView;
import net.halalaboos.engine.api.gui.components.TextField;
import net.halalaboos.engine.api.gui.containers.Window;
import net.halalaboos.engine.api.gui.simple.SimpleContainer;
import net.halalaboos.engine.api.gui.theme.FontRenderer;
import net.halalaboos.engine.api.gui.theme.Theme;
import net.halalaboos.engine.rendering.GLUtils;
import static net.halalaboos.game.ThemeRenderUtils.*;

public class SimpleTheme implements Theme {
	
	private FontRenderer fontRenderer;
	
	public SimpleTheme(FontRenderer fontRenderer) {
		this.fontRenderer = fontRenderer;
	}
	
	@Override
	public void render(Container container, Component component) {
		if (component instanceof Button) {
			Button button = (Button) component;
			glColor(getColorWithAffects(0xFF005500, button.hasMouseOver(), button.hasMouseDown()));
			GLUtils.drawRect(button.getX(), button.getY(), button.getX() + button.getWidth(), button.getY() + button.getHeight());
			fontRenderer.drawCenteredString(button.getTitle(), button.getX() + button.getWidth() / 2 , button.getY() + button.getHeight() / 2, 0xFFFFFFFF);
		} else if (component instanceof TextField) {
			TextField textField = (TextField) component;
			glColor(getColorWithAffects(0xFF005500, textField.hasMouseOver(), textField.hasMouseDown()));
			GLUtils.drawRect(textField.getX(), textField.getY(), textField.getX() + textField.getWidth(), textField.getY() + textField.getHeight());
			String text = textField.getTextForRender(textField.getWidth()) + textField.getCarot();
			fontRenderer.drawString(text, textField.getX() + 2, textField.getY() + textField.getHeight() / 2 - fontRenderer.getStringHeight(text) / 2, 0xFFFFFFFF);
		} else if (component instanceof Label) {
			Label label = (Label) component;
			fontRenderer.drawString(label.getText(), label.getX() + 2, label.getY() + label.getHeight() / 2 - fontRenderer.getStringHeight(label.getText()) / 2, getColorWithAffects(0xFFAAAAAA, label.hasMouseOver(), false));
		} else if (component instanceof ListView) {
			ListView listView = (ListView) component;
			int height = 0;
			for (int i = 0; i < listView.getItems().size(); i++) {
				String toString = listView.getItems().get(i).toString();
				fontRenderer.drawString(toString, listView.getX() + 2, listView.getY() + height, getColorWithAffects(0xFFAAAAAA, listView.hasMouseOver(), false));
				height += fontRenderer.getStringHeight(toString);
			}
		} else if (component instanceof Dropdown) {
			Dropdown dropdown = (Dropdown) component;
			glColor(0xFF000055);
			GLUtils.drawRect(dropdown.getX(), dropdown.getY(), dropdown.getX() + dropdown.getWidth(), dropdown.getY() + dropdown.getHeight());
			fontRenderer.drawCenteredString("S:" + dropdown.getSelectedItem(), dropdown.getX() + dropdown.getWidth() / 2, dropdown.getY() + dropdown.getHeight() / 2, getColorWithAffects(0xFFAAAAAA, dropdown.hasMouseOver(), false));

			if (dropdown.isDown()) {
				int[] dropdownArea = dropdown.getDropdown();
				glColor(0xFF000055);
				GLUtils.drawRect(dropdownArea[0], dropdownArea[1], dropdownArea[0] + dropdownArea[2], dropdownArea[1] + dropdownArea[3]);
				int height = 0;
				for (int i = 0; i < dropdown.getItems().size(); i++) {
					String toString = dropdown.getItems().get(i).toString();
					fontRenderer.drawString(toString, dropdownArea[0] + 2, dropdownArea[1] + height, getColorWithAffects(0xFFAAAAAA, dropdown.hasMouseOver(), false));
					height += dropdown.getItemSize() + dropdown.getItemPadding();
				}
			}
		} else if (component instanceof Window) {
			Window window = (Window) component;
			glColor(0xAA000055);
			drawBorder2(window);
			fontRenderer.drawCenteredString(window.getTitle(), window.getX() + window.getWidth() / 2, window.getY() - window.getTabHeight() / 2, 0xFFFFFFFF);
			drawScrollbars(window);
		}
	}

	
	@Override
	public FontRenderer getFontRenderer() {
		return fontRenderer;
	}

	@Override
	public void setFontRenderer(FontRenderer fontRenderer) {
		this.fontRenderer = fontRenderer;
	}

	@Override
	public void drawTooltip(String tooltip, float x, float y) {
	}

}
