package net.halalaboos.game;

import static org.lwjgl.opengl.GL11.*;

import java.awt.Font;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import net.halalaboos.engine.Engine;
import net.halalaboos.engine.Scene;
import net.halalaboos.engine.Vbo;
import net.halalaboos.engine.api.gui.GuiManager;
import net.halalaboos.engine.api.gui.components.Button;
import net.halalaboos.engine.api.gui.components.Dropdown;
import net.halalaboos.engine.api.gui.components.TextField;
import net.halalaboos.engine.api.gui.containers.Window;
import net.halalaboos.engine.api.gui.simple.SimpleFontRenderer;
import net.halalaboos.engine.api.gui.theme.FontRenderer;

public class GameScene implements Scene {
	
	private Engine engine;
	
	private FontRenderer fontRenderer;
	
	private GuiManager guiManager;
	
	
	@Override
	public void render(Vbo vbo) {
		glClearColor(1F, 1F, 1F, 0F);
		glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		glShadeModel(GL_SMOOTH);        
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_LIGHTING);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		fontRenderer.drawString("vsync: " + engine.isVsync(), 2, 0, 0xFFFFFFFF);
		fontRenderer.drawString("fps: " + engine.getFps(), 2, 20, 0xFFFFFFFF);
		fontRenderer.drawString("x: " + Mouse.getX() + ", y: " + (Display.getHeight() - Mouse.getY() - 1), 2, 40, 0xFFFFFFFF);
		glDisable(GL_TEXTURE_2D);
		guiManager.render();
	}
	
	@Override
	public void setup(Engine engine) {
		this.engine = engine;
		this.fontRenderer = new SimpleFontRenderer(new Font("Lato Semibold", Font.PLAIN, 18), true, false);
		this.guiManager = new GuiManager();
		guiManager.setTheme(new SimpleTheme(fontRenderer));
		final Window window = new Window("Example 1", 64, 64, 512, 512);
		final Dropdown<String> dropdown = new Dropdown<String>(1, 63, 100, 30);

		window.add(dropdown);
		final TextField textField = new TextField(1, 1, 200, 30) {

			@Override
			public void keyTyped(int keyCode, char key) {
				super.keyTyped(keyCode, key);
				if (keyCode == Keyboard.KEY_RETURN) {
					if (hasText()) {
						dropdown.add(getText());
						clear();
					}
				}
			}
		};
		window.add(textField);
		window.add(new Button("Add", 1, 32, 60, 30) {
			@Override
			public void mouseReleased(int x, int y, int buttonId) {
				super.mouseReleased(x, y, buttonId);
				if (textField.hasText()) {
					dropdown.add(textField.getText());
					textField.clear();
				}
			}
		});
		window.pack();
		guiManager.add(window);
		
		Window window2 = new Window("Settings", 1, 1 + window.getTabHeight(), 200, 200);
		window2.add(new Button("Resize", 1, 1, 100, 30) {
			@Override
			public void mouseReleased(int x, int y, int buttonId) {
				super.mouseReleased(x, y, buttonId);
				window.pack();
			}
		});
		window2.pack();
		guiManager.add(window2);
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void keyTyped(int keyCode, char key) {
		guiManager.keyTyped(keyCode, key);
	}

	@Override
	public void mousePressed(int x, int y, int buttonId) {
		guiManager.mousePressed(x, y, buttonId);
	}

	@Override
	public void mouseWheel(int amount) {
		guiManager.mouseWheel(amount);
	}

	@Override
	public void update(float delta) {
	}

	@Override
	public void close() {
	}

	@Override
	public void mouseReleased(int x, int y, int buttonId) {
		guiManager.mouseReleased(x, y, buttonId);
	}

}
